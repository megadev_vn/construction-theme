<?php
/**
 * @file
 * blog_team..inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function blog_team_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_socials_icon';
  $multifield->label = 'field_socials_icon';
  $multifield->description = '';
  $export['field_socials_icon'] = $multifield;

  return $export;
}

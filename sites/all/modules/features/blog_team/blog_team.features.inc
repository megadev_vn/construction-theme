<?php
/**
 * @file
 * blog_team.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blog_team_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "multifield" && $api == "") {
    return array("version" => "");
  }
}

/**
 * Implements hook_views_api().
 */
function blog_team_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function blog_team_image_default_styles() {
  $styles = array();

  // Exported image style: image_blog_fullwidth.
  $styles['image_blog_fullwidth'] = array(
    'label' => 'Image Blog Fullwidth (423x339)',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 423,
          'height' => 340,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_testimonials.
  $styles['image_testimonials'] = array(
    'label' => 'Image Testimonials (133x133)',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 133,
          'height' => 133,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: recent_posts.
  $styles['recent_posts'] = array(
    'label' => 'Recent Posts (50x50)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 50,
          'height' => 50,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function blog_team_node_info() {
  $items = array(
    'blog' => array(
      'name' => t('Blog'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'partners' => array(
      'name' => t('Partners'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'slider' => array(
      'name' => t('Slider'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'team' => array(
      'name' => t('Team'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * blog_team.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function blog_team_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'accordion_icon';
  $bean_type->label = 'Accordion Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['accordion_icon'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'block_service_infomation';
  $bean_type->label = 'Block Service Infomation';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['block_service_infomation'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'infomation_with_icon';
  $bean_type->label = 'Infomation With Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['infomation_with_icon'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'services_block_icon';
  $bean_type->label = 'Services Block Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['services_block_icon'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'socials_icon';
  $bean_type->label = 'Socials Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['socials_icon'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'tabs_icon';
  $bean_type->label = 'Tabs Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['tabs_icon'] = $bean_type;

  return $export;
}

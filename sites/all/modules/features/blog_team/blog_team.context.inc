<?php
/**
 * @file
 * blog_team.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function blog_team_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_us';
  $context->description = '';
  $context->tag = 'Team';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us' => 'about-us',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'construction_custom-block_about_info' => array(
          'module' => 'construction_custom',
          'delta' => 'block_about_info',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-team_style_1-block' => array(
          'module' => 'views',
          'delta' => 'team_style_1-block',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-_partners-block' => array(
          'module' => 'views',
          'delta' => '_partners-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-recent_posts-block_recent_posts' => array(
          'module' => 'views',
          'delta' => 'recent_posts-block_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-icon-info-widget-right' => array(
          'module' => 'bean',
          'delta' => 'icon-info-widget-right',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Team');
  $export['about_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_us_2';
  $context->description = '';
  $context->tag = 'Team';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us-2' => 'about-us-2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'construction_custom-block_about_info' => array(
          'module' => 'construction_custom',
          'delta' => 'block_about_info',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-team_style_2-block' => array(
          'module' => 'views',
          'delta' => 'team_style_2-block',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-_partners-block' => array(
          'module' => 'views',
          'delta' => '_partners-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-recent_posts-block_recent_posts' => array(
          'module' => 'views',
          'delta' => 'recent_posts-block_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-icon-info-widget-right' => array(
          'module' => 'bean',
          'delta' => 'icon-info-widget-right',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Team');
  $export['about_us_2'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_2col';
  $context->description = '';
  $context->tag = 'Blog';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'blog_2_columns:page_blog_2col' => 'blog_2_columns:page_blog_2col',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-recent_posts-block_recent_posts' => array(
          'module' => 'views',
          'delta' => 'recent_posts-block_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-icon-info-widget-right' => array(
          'module' => 'bean',
          'delta' => 'icon-info-widget-right',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');
  $export['blog_2col'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_fullwidth';
  $context->description = '';
  $context->tag = 'Blog';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'blog_fullwidth:page_blog_fullwidth' => 'blog_fullwidth:page_blog_fullwidth',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-recent_posts-block_recent_posts' => array(
          'module' => 'views',
          'delta' => 'recent_posts-block_recent_posts',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-icon-info-widget-right' => array(
          'module' => 'bean',
          'delta' => 'icon-info-widget-right',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');
  $export['blog_fullwidth'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_single';
  $context->description = '';
  $context->tag = 'Blog';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog' => 'blog',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-recent_posts-block_recent_posts' => array(
          'module' => 'views',
          'delta' => 'recent_posts-block_recent_posts',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'bean-icon-info-widget-right' => array(
          'module' => 'bean',
          'delta' => 'icon-info-widget-right',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');
  $export['blog_single'] = $context;

  return $export;
}

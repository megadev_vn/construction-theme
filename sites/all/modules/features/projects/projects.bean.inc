<?php
/**
 * @file
 * projects.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function projects_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'contact_info_footer';
  $bean_type->label = 'Footer : Contact Info';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['contact_info_footer'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'header_social_icon';
  $bean_type->label = 'Header : Social Icon';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['header_social_icon'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'service_block_2';
  $bean_type->label = 'Service Block 2';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['service_block_2'] = $bean_type;

  return $export;
}

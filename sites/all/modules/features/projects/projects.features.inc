<?php
/**
 * @file
 * projects.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function projects_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "multifield" && $api == "") {
    return array("version" => "");
  }
}

/**
 * Implements hook_views_api().
 */
function projects_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function projects_image_default_styles() {
  $styles = array();

  // Exported image style: projects_455x333.
  $styles['projects_455x333'] = array(
    'label' => 'Projects ( 455x333)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 455,
          'height' => 333,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function projects_node_info() {
  $items = array(
    'projects' => array(
      'name' => t('Projects'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

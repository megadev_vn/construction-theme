<?php
/**
 * @file
 * projects.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function projects_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact_us';
  $context->description = '';
  $context->tag = 'Contact';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'webform-client-block-7' => array(
          'module' => 'webform',
          'delta' => 'client-block-7',
          'region' => 'content',
          'weight' => '-9',
        ),
        'construction_custom-contact_text' => array(
          'module' => 'construction_custom',
          'delta' => 'contact_text',
          'region' => 'content',
          'weight' => '-6',
        ),
        'bean-icon-contact-info' => array(
          'module' => 'bean',
          'delta' => 'icon-contact-info',
          'region' => 'content',
          'weight' => '-5',
        ),
        'construction_custom-gmap_cons' => array(
          'module' => 'construction_custom',
          'delta' => 'gmap_cons',
          'region' => 'content',
          'weight' => '-4',
        ),
      ),
    ),
    'template_suggestions' => 'page__contact',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact');
  $export['contact_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact_us_2';
  $context->description = '';
  $context->tag = 'Contact';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact-2' => 'contact-2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'webform-client-block-7' => array(
          'module' => 'webform',
          'delta' => 'client-block-7',
          'region' => 'content',
          'weight' => '-10',
        ),
        'construction_custom-contact_text' => array(
          'module' => 'construction_custom',
          'delta' => 'contact_text',
          'region' => 'content',
          'weight' => '-9',
        ),
        'bean-icon-contact-info' => array(
          'module' => 'bean',
          'delta' => 'icon-contact-info',
          'region' => 'content',
          'weight' => '-8',
        ),
        'construction_custom-gmap_cons' => array(
          'module' => 'construction_custom',
          'delta' => 'gmap_cons',
          'region' => 'content',
          'weight' => '-7',
        ),
      ),
    ),
    'template_suggestions' => 'page__contact_2',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact');
  $export['contact_us_2'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home';
  $context->description = '';
  $context->tag = 'Home';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'home' => 'home',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-slideshow-block' => array(
          'module' => 'views',
          'delta' => 'slideshow-block',
          'region' => 'slider',
          'weight' => '-10',
        ),
        'bean-service-block-home' => array(
          'module' => 'bean',
          'delta' => 'service-block-home',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-project_full_width-block_1' => array(
          'module' => 'views',
          'delta' => 'project_full_width-block_1',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-latest_posts-block' => array(
          'module' => 'views',
          'delta' => 'latest_posts-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-testimonials-block' => array(
          'module' => 'views',
          'delta' => 'testimonials-block',
          'region' => 'content',
          'weight' => '-7',
        ),
        'block-4' => array(
          'module' => 'block',
          'delta' => '4',
          'region' => 'content',
          'weight' => '-6',
        ),
        'bean-accordion-icon-infomation' => array(
          'module' => 'bean',
          'delta' => 'accordion-icon-infomation',
          'region' => 'content',
          'weight' => '-5',
        ),
      ),
    ),
    'template_suggestions' => 'page__home_template',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  $export['home'] = $context;

  return $export;
}

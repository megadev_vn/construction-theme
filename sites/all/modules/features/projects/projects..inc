<?php
/**
 * @file
 * projects..inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function projects_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_footer_contact_icon';
  $multifield->label = 'field_footer_contact_icon';
  $multifield->description = '';
  $export['field_footer_contact_icon'] = $multifield;

  return $export;
}

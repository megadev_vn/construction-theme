<?php
	global $base_url;
	$author_title  		= variable_get('block_block_about_title','');
	$author_des 		= variable_get('block_block_about_des','');
	$image_upload   	= variable_get('block_about_info_img_upload', null);
	if(!empty($image_upload)) {
		$file = file_load($image_upload);
		if($file) {
			$image_path = file_create_url($file->uri);
		}
	}else {
		$image_path = $base_url.'/'.drupal_get_path('theme','construction').'/images/about-us.png';
	}
?>
<div class="aboutus">
<div class="row aboutContent">
	<div class="row aboutUsTexts m0 member">
		<div class="fleft textsPart">
		    <h2><?php print $author_title?></h2>
		    <p><?php print $author_des?></p>
		</div>
		<div class="fleft aboutImg">
		    <img src="<?php print $image_path?>" alt="">
		</div>
	</div>
</div>
</div>
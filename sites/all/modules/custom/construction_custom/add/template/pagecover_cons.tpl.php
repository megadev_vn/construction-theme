<?php
	global $base_url;
	$title  			= variable_get('block_pagecover_title');
	$disable_bc  		= variable_get('block_pagecover_breadcrumb');
	$image_upload   	= variable_get('block_pagecover_bg_img_upload', null);
	if(!empty($image_upload)) {
		$file = file_load($image_upload);
		if($file) {
			$image_path = file_create_url($file->uri);
		}
	}else {
		$image_path = $base_url.'/'.drupal_get_path('theme','construction').'/images/cover.png';
	}
?>
<section id="pageCover" class="row" style="background: url('<?php print $image_path?>') no-repeat scroll center center #f7b71e">
    <div class="row pageTitle"><?php print (($title == '') ? drupal_get_title() : $title )?></div>
	<?php if($disable_bc == FALSE):?>
		<div class="row pageBreadcrumbs">
			<?php
				$bcarr = drupal_get_breadcrumb();
				//$bcarr [] = drupal_get_title();
				print  theme('breadcrumb', array('breadcrumb'=>$bcarr));
			?>
		</div>
	<?php endif;?>
</section>
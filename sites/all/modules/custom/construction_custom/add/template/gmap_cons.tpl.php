<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $base_url;
$location = variable_get('setting_gmap_cons', null);
$title_show = variable_get('setting_gmap_cons_title', null);
$title_marker = variable_get('setting_gmap_cons_desc', null);
$desc_marker = variable_get('setting_gmap_cons_desc_full', null);
?>
<div id="map_canvas"></div>
<script type="text/javascript">
  jQuery(document).ready(function () {
      if (jQuery("#map_canvas").size() > 0) {
          var onMobile = false;
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
              onMobile = true;
          }
          if (onMobile == false) {
              jQuery(window).load(function () {
                  //Google Map
                  var settings = {
                      zoom: 15,
                      center: new google.maps.LatLng(<?php print $location; ?>),
                      mapTypeId: google.maps.MapTypeId.ROADMAP,
                      mapTypeControl: true,
                      scrollwheel: true,
                      draggable: true,
                      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                      navigationControl: false,
                      navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}

                  };
                  var map = new google.maps.Map(document.getElementById("map_canvas"), settings);
                  google.maps.event.addDomListener(window, "resize", function () {
                      var center = map.getCenter();
                      google.maps.event.trigger(map, "resize");
                      map.setCenter(center);
                  });
                  var infowindow = new google.maps.InfoWindow({
                      content: '<div id="content"><h3><?php print $title_marker; ?></h3><div id="bodyContent"><p><?php print $desc_marker; ?></p></div></div>'

                  });
                  var companyPos = new google.maps.LatLng(<?php print $location; ?>);
                  var companyMarker = new google.maps.Marker({
                      position: companyPos,
                      map: map,
                      //icon: companyImage,
                      title: "<?php print $title_show; ?>",
                      zIndex: 3});
                  google.maps.event.addListener(companyMarker, "click", function () {
                      infowindow.open(map, companyMarker);
                  });
              });
          }
      }
  });

</script>
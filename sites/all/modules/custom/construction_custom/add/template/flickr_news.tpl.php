<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $base_url;
$theme_path = drupal_get_path("theme", "construction");
$flick_id  	= variable_get('flickr_news_id', null);
?>
<div class="row flickrSlider">
    <div class="widgetHeader row m0"><img src="<?php print $base_url.'/'. $theme_path;?>/images/whiteSquare.png" alt="">Flickr Gallery</div>        
    <div class="row flickrSliderRow m0">
        <div data-toggle="jsfg" data-tags="dogs" data-per-page="5" data-set-id="<?php print $flick_id;?>"></div>
    </div>                        
</div>    


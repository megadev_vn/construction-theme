(function ($) {

    $(document).ready(function () {

        $(window).load(function () {
            $('#container').isotope({
                // options
                itemSelector: '.project',
                layoutMode: 'masonry'
            });
            $('#filters li').on('click', function () {
                $('#filters').find('.active').removeClass('active');
                $(this).addClass('active');
                $filterValue = $(this).attr('data-filter');
                $('#container').isotope({filter: $filterValue});
            });
            $('#main_nav li.dropdown > a').attr({
                                                'class': 'dropdown-toggle',
                                                'data-toggle': 'dropdown',
                                                'role': 'button',
                                                'aria-expanded': 'false'
                                            });
            var pageHeight = $(window).innerHeight();
            var padding_top = (pageHeight - ($('.page-error').height()))/2;
            $('#page-wrapper.error-page').css({'height': pageHeight, 'padding-top':padding_top});
        });
    });

})(jQuery);

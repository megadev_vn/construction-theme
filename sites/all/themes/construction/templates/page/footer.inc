<footer id="nr_footer" class="row">
    <div class="container">
        <div class="row goTop">
            <a href="#top"><i class="icon fontello icon-angle-up"></i></a>
        </div>
        <div class="row twitterSlide">   
            <?php print render($page['footer']); ?>
        </div>
        <div class="footerWidget row">
            <div class="col-sm-6 widget">
                <?php print render($page['footer_first']); ?>
            </div>
            <div class="col-sm-6 widget">
                <?php print render($page['footer_second']); ?>
            </div>
        </div>
        <div class="row copyrightRow">
            <?php print render($page['footer_copyright']); ?>
        </div>
    </div>
</footer>
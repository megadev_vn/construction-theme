<div id="nr_topStrip" class="row">
    <div class="container">
        <div class="row">
            <div class="list-inline c-info fleft">
                <?php print render($page['header_first']); ?>
            </div>
            <div class="list-inline lang fright">
                <?php print render($page['header_second']); ?>
            </div>
        </div>
    </div>
</div> <!--Top Strip-->

<header class="row">
    <div class="container">
        <div class="row">
            <div class="logo col-sm-6">
                <div class="row">
                    <?php if ($logo): ?>
                      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="gaia"/>
                      </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="social_nav col-sm-6">
                <div class="row">
                    <?php print render($page['header']); ?>
                </div>
            </div>
        </div>
    </div>
</header> <!--Header-->

<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid container">
        <div class="row m04m">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav">
                    <span class="bars">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                    <span class="btn-text"><?php print t('Select Page') ?></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="main_nav">
                <?php if (!empty($primary_nav)): ?>
                  <?php print render($primary_nav); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav> 
<!--Main Nav-->
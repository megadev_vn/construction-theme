<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="row filtersRow m0">
    <button type="button" class="collapsed project_filderButton visible-xs" data-toggle="collapse" data-target="#filters">
        <span class="btn-text"><i class="fa fa-filter"></i> Project Filter</span>
    </button>

    <ul class="list-inline text-center collapse navbar-collapse filters" id="filters">
        <li class="active" data-filter="*"><i class="fa fa-th"></i>Show All</li>
            <?php foreach ($rows as $id => $row): ?>
              <?php print $row; ?>
            <?php endforeach; ?>
    </ul> 
</div>  
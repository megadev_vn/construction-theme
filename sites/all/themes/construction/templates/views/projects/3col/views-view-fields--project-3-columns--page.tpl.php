<?php
/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$img_bg = file_create_url($row->field_field_project_image[0]['rendered']['#item']['uri']);
?>
<div class="project col-md-4 col-sm-6 col-xs-12 <?php print $fields['field_project_category']->content;?>">
    <a href="<?php print $img_bg;?>" data-lightbox="project" data-title="<?php print $fields['title']->content;?> (<?php print $fields['field_project_tag']->content;?>)">
        <img src="<?php print $img_bg;?>" alt="Project 1" class="projectImg">
    </a>
    <div class="projectDetails row m0">
        <div class="fleft nameType">
            <div class="row m0 projectName"><?php print $fields['title']->content;?></div>
            <div class="row m0 projectType"><?php print $fields['field_project_tag']->content;?></div>
        </div>
        <div class="fright projectIcons btn-group" role="group">
            <a href="<?php print $img_bg;?>" data-lightbox="project" data-title="<?php print $fields['title']->content. ' ( ' . $fields['field_project_tag']->content;?>)" class="btn btn-default"><i class="icon fontello icon-search" aria-hidden="true"></i></a>
            <a href="<?php print $fields['path']->content;?>" class="btn btn-default"><i class="icon fontello icon-doc"></i></a>
        </div>
    </div>
</div>

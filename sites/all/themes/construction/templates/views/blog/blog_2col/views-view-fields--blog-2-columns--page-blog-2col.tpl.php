<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="col-sm-6 blog">
    <div class="row m0 blogInner">
        <div class="row m0 blogDateTime">
            <i class="icon fontello icon-calendar"></i> <?php print date('d F Y, H:i A', $row->node_created)?>
        </div>
        <div class="row m0 featureImg">
            <?php print render($fields['field_image_blog']->content); ?>
        </div>
        <div class="row m0 postExcerpts">
            <div class="row m0 postExcerptInner">
                <a href="<?php print ($fields['path']->content); ?>" class="postTitle row m0"><h4><?php print $row->node_title?></h4></a>
                <p><?php print $fields['body']->content?></p>
                <a href="<?php print ($fields['path']->content);?>" class="readMore"><?php print t('read more')?></a>
            </div>
        </div>
    </div>
</div>
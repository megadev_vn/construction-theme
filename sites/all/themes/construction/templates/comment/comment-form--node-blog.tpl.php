<?php
global $base_url;
hide($form['subject']);
hide($form['author']);
hide($form['comment_body']);

unset($form['author']['name']['#theme_wrappers']);
unset($form['author']['mail']['#theme_wrappers']);
unset($form['author']['homepage']['#theme_wrappers']);
unset($form['comment_body']['und'][0]['value']['#theme_wrappers']);
unset($form['actions']['preview']);
$form['actions']['submit']['#value'] = t("post a comment");

?>
<?php  if(user_is_logged_in() == false):?>
<div class="col-sm-6 p0 commenterInfoInputs">
    <div class="row m0">
        <div class="input-group">
            <span class="input-group-addon"><i class="icon fontello icon-user"></i></span>
            <?php print drupal_render($form['author']['name']); ?>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon fontello icon-mail-alt"></i></span>
            <?php print drupal_render($form['author']['mail']); ?>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon fontello icon-link"></i></span>
            <?php print drupal_render($form['author']['homepage']); ?>
        </div>
    </div>
</div>
<?php endif;?>
<div class="<?php print ((user_is_logged_in() == true) ? 'col-sm-12' : 'col-sm-6')?> p0">
    <div class="row m0">
        <div class="input-group">
            <?php print drupal_render($form['comment_body']['und'][0]['value']); ?>
        </div>
        <?php print drupal_render($form['actions']) ?>
    </div>
</div>
<div style="display: none">
  <?php print drupal_render_children($form); ?>
</div>



<?php
/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
$theme_path = drupal_get_path("theme", "construction");
global $base_url;
?>
<div class="getInTouch_widget row">
    <div class="widgetHeader row m0"><img src="<?php print $base_url . "/" . $theme_path; ?>/images/whiteSquare.png" alt="">Get in touch</div>        
    <div class="row getInTouch_tab m0">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="getInTouch_tab">
            <?php
            foreach ($items as $delta => $item):
              unset($item['#theme_wrappers']);
              unset($item['#attributes']);
              unset($item['links']);
              $value = $element['#items'][$delta]['value'];
              $icon = $element[$delta]['entity']['field_collection_item'][$value]['field_footer_icon_contact']['#items'][0]['icon'];
              ?>    
              <li role="presentation" class="<?php if ($delta === 0) {print 'active';} ?>">
                  <a href="#<?php print $icon; ?>" aria-controls="<?php print $icon; ?>" role="tab" data-toggle="tab">
                      <i class="<?php print $icon; ?>"></i>
                  </a>
              </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content footer-contact">
            <?php
            foreach ($items as $delta_2 => $item):
              unset($item['#theme_wrappers']);
              unset($item['#attributes']);
              unset($item['links']);
              $value_2 = $element['#items'][$delta_2]['value'];
              $icon_2 = $items[$delta_2]['entity']['field_collection_item'][$value_2]['field_footer_icon_contact'][0]['#icon'];
              
              ?>
              <div role="tabpanel" class="tab-pane <?php if($delta_2==0): print 'active';endif;?>" id="<?php print $icon_2; ?>">
                  <i class="<?php print $icon_2; ?>"></i><?php print $items[$delta_2]['entity']['field_collection_item'][$value_2]['field_footer_text_contact'][0]['#markup']; ?>
              </div>
          <?php endforeach; ?>
        </div>
    </div>
</div>



<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
$block_chosse_acco 	= get_block_form_id($chosse_block_acco);
$block_chosse_acco_2 = get_block_form_id($chosse_block_acco_2);
$block_chosse_acco_3 = get_block_form_id($chosse_block_acco_3);
?>
<?php if(@$acco_title != '' && @$acco_info != ''): ?>
	<div class="row sectionTitles m0 accordion-tabs">
        <h2 class="sectionTitle"><?php print @$acco_title?></h2>
        <div class="sectionSubTitle"><?php print @$acco_info?></div>
    </div>
<?php endif;?>
<?php if(@$acco_col == '2' || @$acco_col == '3'):?>
<div class="container"><div class="row clearfix">
<?php endif;?>
	<div class="<?php print @$class_col_acco?>">

		<div class="row m0 leftAlignedTap">
            <ul class="nav nav-tabs" role="tablist" id="myTab">
            <?php foreach ($items as $delta => $item):
				unset($item['#theme_wrappers']);
				unset($item['#attributes']);
				unset($item['links']);
				$value = $element['#items'][$delta]['value'];
				$icon = array(
					'#theme' => 'icon',
					'#bundle' => $items[$delta]['entity']['field_collection_item'][$value]['field_tabs_icon'][0]['#bundle'],
					'#icon' => $items[$delta]['entity']['field_collection_item'][$value]['field_tabs_icon'][0]['#icon'],
				);
			?>
                <li role="presentation" class="<?php print (($delta == 0)?'active':'')?>">
                  <a href="#h_tab<?php print ($delta+1)?>" aria-controls="h_tab<?php print ($delta+1)?>" role="tab" data-toggle="tab">
                      <?php print render($icon); ?><?php print ' '.$items[$delta]['entity']['field_collection_item'][$value]['field_tabs_title'][0]['#markup']?>
                  </a>
                </li>
            <?php endforeach; ?>
            </ul>
            <div class="tab-content">
            	<?php foreach ($items as $delta => $item):
					unset($item['#theme_wrappers']);
					unset($item['#attributes']);
					unset($item['links']);
					$value = $element['#items'][$delta]['value'];
				?>
	                <div role="tabpanel" class="tab-pane <?php print (($delta == 0)?'active':'')?>" id="h_tab<?php print ($delta+1)?>">
	                    <?php print $items[$delta]['entity']['field_collection_item'][$value]['field_tabs_description'][0]['#markup']?>
	                </div>
	            <?php endforeach; ?>
            </div>
        </div>
	</div>
	<?php if(@$acco_col == '2' || @$acco_col == '3'):?>
	<div class="<?php print @$class_col_acco?>">
		<?php 
	    	$blockObject = block_load($block_chosse_acco['module'], $block_chosse_acco['delta']);
		  	$block = _block_get_renderable_array(_block_render_blocks(array($blockObject)));
		  	print drupal_render($block);
	    ?>
	    <?php if($acco_col == '3'):?>
	    	<?php 
		    	$blockObject = block_load($block_chosse_acco_2['module'], $block_chosse_acco_2['delta']);
			  	$block = _block_get_renderable_array(_block_render_blocks(array($blockObject)));
			  	print drupal_render($block);
		    ?>
	    <?php endif;?>
	</div>
	<?php endif;?>
	<?php if(@$acco_col == '3'):?>
	<div class="<?php print @$class_col_acco?>">
		<?php 
	    	$blockObject = block_load($block_chosse_acco_3['module'], $block_chosse_acco_3['delta']);
		  	$block = _block_get_renderable_array(_block_render_blocks(array($blockObject)));
		  	print drupal_render($block);
	    ?>
	</div>
	<?php endif;?>
<?php if(@$acco_col == '2' || @$acco_col == '3'):?>
</div></div>
<?php endif;?>


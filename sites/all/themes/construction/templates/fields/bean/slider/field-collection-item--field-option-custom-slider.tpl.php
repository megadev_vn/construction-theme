<?php
/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
//dsm($content);
(isset($content['field_offset_x'])) ? $data_x = $content['field_offset_x'][0]['#markup'] : $data_x = 'center';
(isset($content['field_offset_y'])) ? $data_y = $content['field_offset_y'][0]['#markup'] : $data_y = 'center';
(isset($content['field_item_speed'])) ? $speed = $content['field_item_speed'][0]['#markup'] : $speed = 0;
(isset($content['field_item_time_start'])) ? $start = $content['field_item_time_start'][0]['#markup'] : $start = 0;
(isset($content['field_custom_item_slider'])) ? $item_slide = $content['field_custom_item_slider'][0]['#markup'] : $item_slide = '';
switch ($content['field_skill_effect']['#items'][0]['value']) {
	case 'skewfromright':
		$class_skill = 'skewfromright skewtoright';
		break;
	case 'skewfromleft':
		$class_skill = 'skewfromleft skewtoleft';
		break;
	case 'sfr':
		$class_skill = 'sfr str';
		break;
	case 'sfl':
		$class_skill = 'sfl stl';
		break;
	case 'sft':
		$class_skill = 'sft stt';
		break;
	case 'sfb':
		$class_skill = 'sfb stb';
		break;

}
?>
<div class="caption <?php print $class_skill?>"
	data-x="<?php print $data_x?>"
	data-y="<?php print $data_y?>"
	data-speed="<?php print $speed?>"
	data-start="<?php print $start?>"
	data-easing="easeOutBack">
	<?php print $item_slide?>
</div>

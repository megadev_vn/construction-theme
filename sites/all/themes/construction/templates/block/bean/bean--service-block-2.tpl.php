<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="services-page row m0 whatOffer whatOfferInner">
    <div class="row m0 whatOffer">
        <div class="col-sm-12">
            <div class="row m0 whatOfferInner">
                <?php print render($content['field_text_service_2']); ?>
            </div>
        </div>
    </div>
</div>
<div class="services-page row m0 whatOffer">
    <div class="row m0">
        <div class="col-sm-12">
            <div class="row m0 serviceFeature">
                <div class="col-sm-4">
                    <?php print render($content['field_image_service_2']); ?>
                </div>
                <div class="col-sm-8">
                    <?php print render($content['field_icon_service_2']); ?>
                </div>
            </div>
        </div>
    </div>
</div>
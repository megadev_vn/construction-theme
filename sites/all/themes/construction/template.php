<?php

include_once './' . drupal_get_path('theme', 'construction') . '/include/preprocess.inc';

function construction_links__locale_block(&$vars) {
  foreach ($vars['links'] as $language => $langInfo) {
    $name = $langInfo['language']->native;
    $lang = $langInfo['language']->language;
    $vars['links'][$language]['title'] = $name;
    $vars['links'][$language]['attributes']['xml:lang'] = $lang;
    $vars['links'][$language]['attributes']['lang'] = $lang;
    $vars['links'][$language]['html'] = TRUE;
  }
  $content = theme_links($vars);
  return $content;
}

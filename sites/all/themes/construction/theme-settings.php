<?php

function construction_form_system_theme_settings_alter(&$form, $form_state) {

  $theme_path = drupal_get_path('theme', 'construction');
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('Theme settings'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attached' => array(
      'css' => array(drupal_get_path('theme', 'construction') . '/theme-setting/css/admin.css'),
      'js' => array(
        drupal_get_path('theme', 'construction') . '/theme-setting/js/admin.js',
      ),
    ),
  );


  // --------------------------------------------
  // Preload
  // --------------------------------------------
  $form['settings']['preload'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preload'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['preload']['preload_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Preload'),
      '#default_value' => theme_get_setting('preload_enable', 'construction')
  );
  $form['settings']['preload']['img_preload'] = array(
    '#title' => t('Preload Image'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://img-preload/',
    '#default_value' => theme_get_setting('img_preload', 'construction'),
  );

  // --------------------------------------------
  // BLOG SETTING
  // --------------------------------------------
  $form['settings']['blog'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blog Setting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['blog']['blog_singer_setting'] = array(
    '#type' => 'select',
    '#title' => t('Select Blog Detail Setting'),
    '#options' => array(
      'left' => 'Blog Sidebar Left',
      'right' => 'Blog Sidebar Right',
      'full' => 'Blog Fullwidth',
    ),
    '#default_value' => theme_get_setting('blog_singer_setting', 'construction'),
  );
  // --------------------------------------------
  // CONTACT SETTING
  // --------------------------------------------
  $form['settings']['contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Setting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['contact']['choose_webform'] = array(
    '#type' => 'textfield',
    '#title' => t('Input id webform for contact page'),
    '#default_value' => theme_get_setting('choose_webform', '0'),
  );

  // --------------------------------------------
  // Custom CSS
  // --------------------------------------------
  $form['settings']['custom_css'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom CSS'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['custom_css']['custom_css'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom CSS'),
    '#default_value' => theme_get_setting('custom_css', 'construction'),
    '#description' => t('<strong>Example:</strong><br/>h1 { font-family: \'Metrophobic\', Arial, serif; font-weight: 400; }')
  );

  // --------------------------------------------
  // 404 Not Found
  // --------------------------------------------
  $form['settings']['notfound'] = array(
    '#type' => 'fieldset',
    '#title' => t('404 Not Found'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['notfound']['not_found_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Not Found Title'),
    '#default_value' => theme_get_setting('not_found_title', 'construction'),
  );
  $form['settings']['notfound']['not_found_description'] = array(
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#title' => t('Not Found Description'),
    '#default_value' => theme_get_setting('not_found_description', 'construction'),
  );

  // --------------------------------------------
  // 403 Access Denied
  // --------------------------------------------
  $form['settings']['access_denied'] = array(
    '#type' => 'fieldset',
    '#title' => t('403 Access Denied'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['access_denied']['access_denied_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Denied Title'),
    '#default_value' => theme_get_setting('access_denied_title', 'construction'),
  );
  $form['settings']['access_denied']['access_denied_description'] = array(
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#title' => t('Access Denied Description'),
    '#default_value' => theme_get_setting('access_denied_description', 'construction'),
  );
}

//test deploy hhsđ
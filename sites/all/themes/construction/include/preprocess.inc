<?php

function base_url() {
  global $base_url;
  return $base_url;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////      HTML PREPROCESS    /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Implement hook_preprocess_html()
 */
function construction_preprocess_html(&$vars) {
  global $base_url;
  $css = "";
  $scripts = "";

  $path_theme = drupal_get_path('theme', 'construction');

  //  drupal_add_js();
  drupal_add_js("{$path_theme}/js/libs/bootstrap.min.js");
  drupal_add_js("{$path_theme}/js/libs/jquery.themepunch.tools.min.js");
  drupal_add_js("{$path_theme}/js/libs/jquery.themepunch.revolution.min.js");
  drupal_add_js("{$path_theme}/js/libs/owl.carousel.min.js");
  drupal_add_js("{$path_theme}/js/libs/jquery.nicescroll.js");
  drupal_add_js("{$path_theme}/js/libs/js-flickr-gallery.min.js");
  drupal_add_js("{$path_theme}/js/libs/lightbox.min.js");
  drupal_add_js("{$path_theme}/js/libs/isotope-custom.js");

  drupal_add_js("{$path_theme}/js/construction.js");
  drupal_add_js("{$path_theme}/js/custom.js");


  //  drupal_add_css();

  drupal_add_css("{$path_theme}/css/libs/bootstrap.min.css");
  drupal_add_css("{$path_theme}/css/libs/bootstrap-theme.min.css");
  drupal_add_css("{$path_theme}/css/libs/settings.css");
  drupal_add_css("{$path_theme}/css/libs/owl.carousel.css");
  drupal_add_css("{$path_theme}/css/libs/js-flickr-gallery.css");
  drupal_add_css("{$path_theme}/css/libs/lightbox.css");
  drupal_add_css("{$path_theme}/css/style.css");
  drupal_add_css("{$path_theme}/css/construction.css");

  /*   * *********************************************
    Theme Settings
   * ********************************************** */

  // Preload
  $img_preload = '';$vars['preload'] = '';
  $pic_fid = theme_get_setting('img_preload', 'construction');
  if(!empty($pic_fid)){
    $file = file_load($pic_fid);
    if($file) {
      $img_preload = file_create_url($file->uri);
    }else{
      $img_preload = $base_url . '/' . $path_theme . '/images/loader.gif';
    }
  }else{
    $img_preload = $base_url . '/' . $path_theme . '/images/loader.gif';
  }
  if (theme_get_setting('preload_enable') == TRUE) {
    $vars['preload'] = '<div id="pageloader" class="row m0">
                            <div class="loader-item"><img src="'.$img_preload.'" alt="loading"></div>
                        </div>';
  }else{
    $vars['preload'] = '';
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////      PAGE PREPROCESS    /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Implements hook_preprocess_page().
 */
function construction_preprocess_page(&$vars, $hook) {

  if ($vars['main_menu']) {
    $vars['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    $vars['primary_nav']['#theme_wrappers'] = array('menu_tree__main_menu');
  }

  global $base_url;
  $args = arg();
  $node_type = menu_get_object();
  // Hide title front, type 
  if ($vars['is_front']) {
    $vars['title'] = '';
  }

  if ($args[0] == 'taxonomy' && $args[1] == 'term' && is_numeric($args[2])) {
    $vars['page']['content']['system_main']['nodes']['#prefix'] = '<div class="list_teaser clearfix">';
    $vars['page']['content']['system_main']['nodes']['#suffix'] = '</div>';
  }

  if ($args[0] == 'comment' && $args[1] == 'reply') {
    $node = node_load($args[2]);
    $vars['theme_hook_suggestions'][] = 'page__comment__reply__type__' . $node->type;
  }

  //Hook suggestion
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__node__type__' . $vars['node']->type;
  }


  if (($views_page = views_get_page_view()) && $views_page->name === "project_full_width") {
            $vars['theme_hook_suggestions'][] = 'page__project_full_width';
  }
  if (($views_page = views_get_page_view()) && $views_page->name === "project_2_collumns") {
            $vars['theme_hook_suggestions'][] = 'page__project_2col';
  }
  if (($views_page = views_get_page_view()) && $views_page->name === "project_3_columns") {
            $vars['theme_hook_suggestions'][] = 'page__project_3col';
  }

  $views_page = views_get_page_view();
  if(isset($views_page) && $views_page->name == 'contact_us'){
      $vars['theme_hook_suggestions'][] = 'page__contact';
  }

  // Theme-setting Not-found
  $vars['not_found_title'] = theme_get_setting('not_found_title') ? theme_get_setting('not_found_title') : 'Oppp!! Page not found !';
  $vars['not_found_description'] = theme_get_setting('not_found_description') ? theme_get_setting('not_found_description') : 'The document you are looking for may have been removed or re-named. </br>Please contact the web site owner for further assistance.';

  // Theme-setting Access Denied
  $vars['access_denied_title'] = theme_get_setting('access_denied_title') ? theme_get_setting('access_denied_title') : 'ACCESS DENIED !';
  $vars['access_denied_description'] = theme_get_setting('access_denied_description') ? theme_get_setting('access_denied_description') : 'A web server may return a 403 Forbidden HTTP status code in response to a request from a client for a web page or resource to indicate that the server can be reached and understood the request, but refuses to take any further action. Status code 403 responses are the result of the web server being configured to deny access, for some reason, to the requested resource by the client.';

  // Theme-Settings Blog Single
  $vars['blog_single_left'] = '';
  $vars['blog_single_right'] = '';
  if (theme_get_setting('blog_singer_setting') == 'full') {
      $vars['blog_single_right'] = 'none';
  } elseif (theme_get_setting('blog_singer_setting') == 'left') {
      $vars['blog_single_left'] = 'col-md-9 col-md-push-3';
      $vars['blog_single_right'] = 'col-md-3 col-md-pull-9 hidden-sm hidden-xs sidebar-left';
  } else {
      $vars['blog_single_right'] = 'col-md-3 hidden-sm hidden-xs sidebar-right';
      $vars['blog_single_left'] = 'col-md-9';
  }
  // CALL ICON
   $icon_call[] = array(
    '#theme' => 'icon',
    '#bundle' => 'font_awesome',
    '#icon' => 'icon-doc',
  );
  drupal_render($icon_call);
  
  $vars['choose_webform'] = theme_get_setting('choose_webform');

}

function construction_preprocess_maintenance_page(&$vars) {
  global $base_url;

  drupal_add_css(path_to_theme() . '/css/construction.css');

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////      FUNCTION PREPROCESS    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Overwrite theme_pager()
 */
function construction_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.
  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('«')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('»')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('active'),
            'data' => '<a href="#">' . $i . '</a>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    $output = '<div class="row paginationRow m0 clearfix"><div class="paginationInner m0 row"><ul class="pagination fleft">';
    foreach ($items as $item) {
      $output .= '<li class="' . $item['class'][0] . '">' . $item['data'] . "</li>\n";
    }
    $output .= '</ul></div></div>';

    return $output;
  }
}

/**
 * Overwrite theme_textarea().
 */
function construction_textarea($variables) {
  $element = $variables['element'];
  $element['#attributes']['placeholder'] = $element['#title'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-control','control-textarea'));

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
    $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
    $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
    $output .= '</div>';
    return $output;
  }
  else {
    $output = '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
    return $output;
  }
}

/**
 * Overwrite theme_textfield().
 */
function construction_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  if (isset($element['#title'])){
    $element['#attributes']['placeholder'] = $element['#title'];
  }
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-control'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output . $extra;
}


/**
 * Overwrite theme_menu_local_tasks()
 * Style for menu T
 * @param $variables
 * @return string
 */

function construction_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables ['primary'])) {
    $variables ['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables ['primary']['#prefix'] .= '<ul class="tabs tab_primary tabs-content nav nav-tabs">';
    $variables ['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables ['primary']);
  }
  if (!empty($variables ['secondary'])) {
    $variables ['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables ['secondary']['#prefix'] .= '<ul class="tabs tab_secondary tabs-content nav nav-tabs">';
    $variables ['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables ['secondary']);
  }

  return $output;
}

//Bread crumb
function construction_breadcrumb($variables) {
    $crumbs = '<ul class="breadcrumb">';
    $breadcrumb = $variables['breadcrumb'];
    if (!empty($breadcrumb)) {
        $breadcrumb[0] = '<a href="' . url('<front>', array('absolute' => TRUE)) . '">Home</a>';
        foreach ($breadcrumb as $value) {
            $crumbs .= '<li>' . $value . '</li>';
        }
        $crumbs .= '<li class="active">' . drupal_get_title() . '</li>';

        $crumbs .='</ul>';
        return $crumbs;
    } else {
        return NULL;
    }
}

/**************************************
*  Messenger 
***************************************/
function construction_status_messages($variables) {
  $display = $variables ['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    //dsm($type);
    $output .= '<div class="container messages-content"><div class="row">';
    if($type=='status'){
          $output .= "<div class=\"alert alert-success\">\n";
          if (!empty($status_heading [$type])) {
            $output .= '<h2 class="element-invisible">' . $status_heading [$type] . "</h2>\n";
          }
            if (count($messages) > 1) {
              $output .= " <ul class=\"list-unstyled\">\n";
              foreach ($messages as $message) {
                $output .= '  <li> ' . $message . "</li>\n";
              }
              $output .= " </ul>\n";
            }
            else {
              $output .= reset($messages);
            }
          $output .= "</div>\n";  
    }
    elseif($type=="error"){
          $output .= "<div class=\"alert alert-danger\">\n";
          if (!empty($status_heading [$type])) {
            $output .= '<h2 class="element-invisible">' . $status_heading [$type] . "</h2>\n";
          }
            if (count($messages) > 1) {
              $output .= " <ul class=\"list-unstyled\">\n";
              foreach ($messages as $message) {
                $output .= '  <li> ' . $message . "</li>\n";
              }
              $output .= " </ul>\n";
            }
            else {
              $output .= reset($messages);
            }
          $output .= "</div>\n";  
    }
    elseif($type=="warning"){
          $output .= "<div class=\"alert alert-warning\">\n";
          if (!empty($status_heading [$type])) {
            $output .= '<h2 class="element-invisible">' . $status_heading [$type] . "</h2>\n";
          }
            if (count($messages) > 1) {
              $output .= " <ul class=\"list-unstyled\">\n";
              foreach ($messages as $message) {
                $output .= ' <li> ' . $message . "</li>\n";
              }
              $output .= " </ul>\n";
            }
            else {
              $output .= reset($messages);
            }
          $output .= "</div>\n";  
    }
    $output .= "</div></div>\n";
  }
  return $output;
}

/**
 * Implement hook_form_alter()
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function construction_form_alter(&$form, &$form_state, $form_id) {
  if (($form_id == 'user_login') || ($form_id == 'user_register_form') || ($form_id == 'user_pass')) {
      $form['pass']['#attributes'] = array('class' => array('form-control'));
      $form['actions']['submit']['#attributes'] = array('class' => array('btn','btn-primary'));
  }
  else{
  switch ($form_id) {
      case 'comment_node_blog_form':
        $form['actions']['submit']['#attributes']['class'] = array(
          'btn',
          'btn-default'
        );
        $form['author']['homepage']['#title'] = t('Website');
        //unset($form['author']['mail']['#description']);
        break;
      case 'search_form':
        $form['basic']['keys']['#title'] = t("New Search");
        $form['basic']['keys']['#description'] = t("If you are not happy with the results bellow please do another search.");
        $form['advanced']['submit']['#attributes'] = array('class' => array('btn','btn-primary'));
        break;
    }
  }
}

/**
 * Implement hook_theme()
 */
function construction_theme() {
  $themes = array();
  $path = drupal_get_path('theme', 'construction') . '/templates';

  $themes['comment_form__node_blog'] = array(
    'render element' => 'form',
    'template' => 'comment-form--node-blog',
    'path' => $path . '/comment',
  );
  $themes['contact_site_form'] = array(
    'render element' => 'form',
    'template' => 'contact-us--form',
    'path' => $path . '/comment',
  );

  return $themes;
}

/**
 * Implement hook_preprocess_comment()
 */
function construction_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment'];

  $variables['custom_date']   = date('F.j.o, H:i A',$comment->created);

  $uri = entity_uri('comment', $comment);
  $uri['options'] += array('attributes' => array('class' => 'permalink', 'rel' => 'bookmark'));

}

/**
 * Implement hook_preprocess_webform_form()
 */
function construction_preprocess_webform_form(&$vars) {
  $info = theme_get_setting('address_info');
  $info = explode('||', $info);
  array_pop($info);
  foreach ($info as $key => $value) {
    $info[$key] = $key != 0 ? substr($value, 1, -1) : substr($value, 0, -1);
  }
  $vars['info'] = array_chunk($info, 4);
  $path = drupal_get_path('module', 'md_block_custom');
  drupal_add_js('https://maps.googleapis.com/maps/api/js?v=3.exp');
  //drupal_add_js($path . '/add_more/js/mdmaps.js');
}

/**
 * Overwirte template_button()
 */
function construction_button($variables) {
    $element = $variables['element'];
    $element['#attributes']['type'] = 'submit';
    element_set_attributes($element, array('id', 'name', 'value'));

    if (!isset($element['#attributes']['class']) || !in_array("ts-button", $element['#attributes']['class'])) {
      $element['#attributes']['class'][] = 'btn btn-primary form-' . $element['#button_type'];
    }
    if (!empty($element['#attributes']['disabled'])) {
        $element['#attributes']['class'][] = 'form-button-disabled';
    }
    return '<input' . drupal_attributes($element['#attributes']) . ' />';
}


/**
 * Overwire theme_username()
 */
function construction_username($variables) {
  if (isset($variables ['link_path'])) {
    $output = l($variables ['name'] . $variables ['extra'], $variables ['link_path'], $variables ['link_options']);
  }
  else {
    $output = '<span>' . $variables ['name'] . $variables ['extra'] . '</span>';
  }
  return $output;
}

/**
 * Overwire menu_tree__main_menu()
 */
function construction_menu_tree__main_menu(&$variables) {
  return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * Overwire menu_link()
 */
function construction_menu_link(array $variables) {
  $element = $variables['element'];
  if ($element['#original_link']['menu_name'] == 'main-menu') {
    $sub_menu = '';
    if ($element['#below']) {
      $element['#attributes']['class'][] = 'dropdown';
      if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
        $sub_menu = drupal_render($element['#below']);
      }
      elseif (!empty($element['#original_link']['depth'])) {
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '';
        $sub_menu .= '<ul class="dropdown-menu" role="menu">' . drupal_render($element['#below']) . '</ul>';
        $element['#localized_options']['html'] = TRUE;
      }
    }

    // Add class Active
    if (($element['#original_link']['in_active_trail'] == TRUE) || ($element['#href'] == '<front>' && drupal_is_front_page())) {
      $element['#attributes']['class'][] = 'active';
    }

    if ($element['#href'] == '<front>' && isset($element['#localized_options']['fragment'])) {
      $output = l(t($element['#title']), '/', $element['#localized_options']);
    }
    else {
      $output = l(t($element['#title']), $element['#href'], $element['#localized_options']);
    }
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }
  
}

